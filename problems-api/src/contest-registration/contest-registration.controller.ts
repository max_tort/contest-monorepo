import { Body, Controller, HttpException, Post, Request, UseGuards } from "@nestjs/common";
import { JwtAuthGuard } from "src/auth/jwt-auth.guard";
import { ContestService } from "src/contest/contest.service";
import { UserService } from "src/user/user.service";
import { ContestRegistrationService } from "./contest-registration.service";

@Controller('registration')
export class ContestRegistrationController {
  constructor(
    private contestService: ContestService, 
    private userService: UserService,
    private contestRegistrationService: ContestRegistrationService
  ) { }

  @UseGuards(JwtAuthGuard)
  @Post('/register')
  async register(@Request() request, @Body() { contestID }: { contestID: string }): Promise<void> {
    const user = await this.userService.findOneByEmail(request.user.email)
    if (user) {
      const contest = await this.contestService.findByID(contestID)
      if (contest) {
        try {
          await this.contestRegistrationService.registerUserToContest(user, contest)
        }
        catch (error) {
          throw new HttpException(error, 400)
        }
      }
      else throw new HttpException(`Contest ${contestID} doesn't exist`, 400)
    } else throw new HttpException(`User with email ${request.user.email} doesn't exist.`, 401)
    return
  }

}