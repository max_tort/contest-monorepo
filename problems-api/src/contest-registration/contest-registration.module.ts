import { Module } from "@nestjs/common";
import { AuthModule } from "src/auth/auth.module";
import { ContestModule } from "src/contest/contest.module";
import { UserModule } from "src/user/user.module";
import { ContestRegistrationController } from "./contest-registration.controller";
import { ContestRegistrationService } from "./contest-registration.service";


@Module({
  imports: [UserModule, ContestModule, AuthModule],
  providers: [ContestRegistrationService],
  controllers: [ContestRegistrationController],
  exports: [ContestRegistrationService]
})
export class ContestRegistrationModule { }