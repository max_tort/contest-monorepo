import { Injectable } from "@nestjs/common";
import { Schema } from "mongoose";
import { ContestDocument } from "src/contest/contest.schema";
import { ContestService } from "src/contest/contest.service";
import { LevelService } from "src/level/level.service";
import { UserDocument } from "src/user/user.schema";
import { UserService } from "src/user/user.service";


@Injectable()
export class ContestRegistrationService {
  constructor(
    private userService: UserService,
    private contestService: ContestService,
  ) { }

  isUserRegistered(user: UserDocument, contestID: string): boolean {
    return user.contestResults.some(element => {
      return element.contestID === contestID
    })
  }

  async registerUserToContest(user: UserDocument, contest: ContestDocument): Promise<UserDocument> {
    if (this.isUserRegistered(user, contest._id.toString())) {
      throw new Error('user is already registered')
    } else {
      user.contestResults.push({
        contestID: contest._id.toString(),
        points: 0,
        levelResults: []
      })
      return await user.save()
    }
  }
}