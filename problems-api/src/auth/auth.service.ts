import { Injectable } from "@nestjs/common";
import { UserService } from "src/user/user.service";
import { hash, compare } from 'bcrypt'
import { CreateUserDTO } from "src/user/dto/user.dto";
import { User } from "src/user/user.schema";


@Injectable()
export class AuthService {
  constructor(private userService: UserService) { }

  async validateUser(userDTO: CreateUserDTO): Promise<User> {
    const user = await this.userService.findOneByEmail(userDTO.email)
    if (user) {
      return user
    } else {
      return await this.userService.createUser(userDTO)
    }
  }
}