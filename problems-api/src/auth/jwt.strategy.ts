import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from 'passport-jwt';

export type DecodedUser = { email: string, firstName: string, lastName: string }

@Injectable()
export class JWTStrategy extends PassportStrategy(Strategy) {
  constructor(configService: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ingoreExpiration: false,
      secretOrKey: configService.get('JWT_SECRET_KEY')
    })
  }

  async validate(payload: any): Promise<DecodedUser> {
    return { email: payload.data.email, firstName: payload.data.firstName, lastName: payload.data.lastName }
  }
}