import { Controller, UseGuards, Request, Get, Param, HttpException} from "@nestjs/common"
import { request } from "express";
import { User } from "src/user/user.schema";
import { UserService } from "src/user/user.service";
import { JwtAuthGuard } from "./jwt-auth.guard";

@Controller('/auth')
export class AuthController {

  constructor(private userService: UserService) {}

  @UseGuards(JwtAuthGuard)
  @Get('get')
  async get(@Request() request): Promise<User> {
    const user = await this.userService.findOneByEmail(request.user.email)
    if (user) {
      return user
    } else {
      return await this.userService.createUser({
        email: request.user.email,
        firstName: request.user.firstName,
        lastName: request.user.lastName,
        grade: ''
      })
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get('setGrade/:grade')
  async setGrade(@Request() request, @Param('grade') grade: string): Promise<User> {
    const user  = await this.userService.findOneByEmail(request.user.email)
    if (user) {
      user.grade = grade
      return await user.save()
    } else throw new HttpException('user does not exist', 401)
  }
}