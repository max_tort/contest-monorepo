import { Module } from "@nestjs/common";
import { AuthModule } from "src/auth/auth.module";
import { ContestModule } from "src/contest/contest.module";
import { LevelModule } from "src/level/level.module";
import { UserModule } from "src/user/user.module";
import { RankController } from "./rank.controller";
import { RankService } from "./rank.service";


@Module({
  imports: [UserModule, LevelModule, ContestModule, AuthModule],
  providers: [RankService],
  controllers: [RankController]
})
export class RankModule { }