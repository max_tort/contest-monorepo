import { Controller, Get, HttpException, Param, Request, UseGuards } from "@nestjs/common";
import { request } from "express";
import { JwtAuthGuard } from "src/auth/jwt-auth.guard";
import { ContestService } from "src/contest/contest.service";
import { Level } from "src/level/level.schema";
import { LevelService } from "src/level/level.service";
import { UserService } from "src/user/user.service";
import { RankService } from "./rank.service";

@Controller('rank')
export class RankController {
  constructor(
    private userService: UserService, 
    private levelService: LevelService, 
    private contestService: ContestService,
    private rankService: RankService
  ) { }

  @UseGuards(JwtAuthGuard)
  @Get('getlvls/:contestID')
  async getLvls(@Request() request, @Param('contestID') contestID: string): Promise<any> {
    try {
      const user = await this.userService.findOneByEmail(request.user.email)
      if (user) {
        return user.contestResults.find(e => e.contestID === contestID)
      } else throw new Error('user does not exist')
    }
    catch(error) {
      throw new HttpException(error.msg, 400)
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get('submit/:levelID')
  async submit(@Request() request, @Param('levelID') levelID: string): Promise<void> {
    try {
      const user = await this.userService.findOneByEmail(request.user.email)
      if (user) {
        const level = await this.levelService.findOneByID(levelID)
        if (level) {
          await this.rankService.submitLevel(user, level.contestID, level)
        }
      } else throw new Error('user does not exist')
    }
    catch(error) {
      throw new HttpException(error.msg, 400)
    }
  }
}