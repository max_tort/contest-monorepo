import { Injectable } from "@nestjs/common";
import { Contest, ContestDocument } from "src/contest/contest.schema";
import { ContestService } from "src/contest/contest.service";
import { LevelDocument } from "src/level/level.schema";
import { LevelService } from "src/level/level.service";
import { User, UserDocument } from "src/user/user.schema";
import { UserService } from "src/user/user.service";

@Injectable()
export class RankService {

  constructor(private levelService: LevelService) { }

  // async getLevels(user: UserDocument, contest: Contest): Promise<Level[]> {
  //   const currentTime = Date.now()
    
  // }

  async submitLevel(user: UserDocument, contestID: string, level: LevelDocument): Promise<User> {
    const levelID: string = level._id.toString()
    const userContest = user.contestResults.findIndex(element => element.contestID === contestID)
    if (user.contestResults[userContest]) {
      if (user.contestResults[userContest].levelResults.includes(levelID)) {
        throw new Error('This Level is already solved')
      } else {
        user.contestResults[userContest].points += level.weight
        user.contestResults[userContest].levelResults.push(levelID)
        user.markModified('contestResults')
        return await user.save()
      }
    } else throw new Error('Contest is not found in the registered contests')
  }
}