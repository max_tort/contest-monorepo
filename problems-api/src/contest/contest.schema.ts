import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type ContestDocument = Contest & Document

export class ContestDTO {
  name: string
  description: string
  startTime: string
  endTime: string
}

export type ContestGrade = '5-6' | '7-8' | '9-11' | 'none'

@Schema()
export class Contest {
  
  @Prop({ required: true })
  name: string

  @Prop()
  description: string

  @Prop({ required: true})
  startTime: Date

  @Prop({ required: true})
  endTime: Date

  @Prop({ default: 0 })
  cost: number
  
}

export const ContestSchema = SchemaFactory.createForClass(Contest)