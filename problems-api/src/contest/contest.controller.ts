import { Body, Controller, Get, Param, Post, Request, UseGuards } from "@nestjs/common";
import { JwtAuthGuard } from "src/auth/jwt-auth.guard";
import { Contest } from "./contest.schema";
import { ContestService } from "./contest.service";

@Controller('contest')
export class ContestController {
  constructor(private contestService: ContestService) {}

  @Get('findByID/:_id')
  async findByID(@Param('_id') _id: string): Promise<Contest | null> {
    return await this.contestService.findByID(_id)
  }

  @Get('findAll')
  async findAllIDs(): Promise<string[]> {
    return await this.contestService.findAllIDs()
  }

  @Post('create')
  async create(@Body() contest: Contest): Promise<Contest> {
    return await this.contestService.create(contest)
  }

  // @UseGuards(JwtAuthGuard)
  // @Post('register')
  // async register(@Body() contest: Contest, @Request() request): Promise<string> {
  //   const user = await this.userService()
  // }
}