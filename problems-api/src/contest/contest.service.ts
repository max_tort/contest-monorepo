import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { UserService } from "src/user/user.service";
import { Contest, ContestDocument } from "./contest.schema";

@Injectable()
export class ContestService {
  constructor(
    @InjectModel(Contest.name) private contestModel: Model<ContestDocument>
  ) { }

  async findByID(_id: string): Promise<ContestDocument | null> {
    return await this.contestModel.findById(_id).lean()
  }

  async findAll(): Promise<Contest[]> {
    return await this.contestModel.find({}).lean()
  }

  async findAllIDs(): Promise<string[]> {
    const contests: ContestDocument[] = await this.contestModel.find({}, '_id').lean()
    const ids: string[] = contests.map(element => (element._id))
    return ids
  }

  async create(contest: Contest): Promise<Contest> {
    const newContest = new this.contestModel(contest)
    return await newContest.save()
  }
}