import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { UserModule } from "src/user/user.module";
import { ContestController } from "./contest.controller";
import { Contest, ContestSchema } from "./contest.schema";
import { ContestService } from "./contest.service";

@Module({
  imports: [MongooseModule.forFeature([{name: Contest.name, schema: ContestSchema}], 'native')],
  providers: [ContestService],
  controllers: [ContestController],
  exports: [ContestService]
})
export class ContestModule {}