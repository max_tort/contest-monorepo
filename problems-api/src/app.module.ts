import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { ContestRegistrationModule } from './contest-registration/contest-registration.module';
import { ContestModule } from './contest/contest.module';
import { LevelModule } from './level/level.module';
import { PaymentModule } from './payment/payment.module';
import { RankModule } from './rank/rank.module';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true
    }),

    // Serve Static
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
      renderPath: '/designer',
    }),

    // Mongoose uri is specified using ConfigModule
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        uri: configService.get('NATIVE_DATABASE_URL'),
        useCreateIndex: true
      }),
      inject: [ConfigService],
      connectionName: 'native'
    }),
    // Custom Feature Modules
    // UserModule,
    AuthModule,
    LevelModule,
    ContestModule,
    ContestRegistrationModule,
    RankModule,
    // PaymentModule
  ],
  providers: [AppService],
})

export class AppModule { }
