import { Controller, Get, Param } from "@nestjs/common";
import { User } from "./user.schema";
import { UserService } from "./user.service";

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get(':email')
  async findUser(@Param('email') email: string): Promise<User> {
    return (await this.userService.findOneByEmail(email))
  }

  @Get()
  async findUsers(): Promise<User[]> {
    return await this.userService.findAll()
  }
}