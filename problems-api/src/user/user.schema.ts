import { Prop, SchemaFactory, Schema as MongooseSchema } from "@nestjs/mongoose";
import { Document, Schema } from "mongoose";

export type UserDocument = User & Document

export class ContestResult {
  contestID: string
  levelResults: string[]
  points: number
}

@MongooseSchema()
export class User {

  @Prop({ required: true, unique: true, lowercase: true })
  email: string
  
  @Prop({ required: true })
  firstName: string

  @Prop({ required: true })
  lastName: string

  @Prop()
  grade: string

  @Prop([ContestResult])
  contestResults: ContestResult[]
}

export const UserSchema = SchemaFactory.createForClass(User)
