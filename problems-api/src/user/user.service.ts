import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CreateUserDTO } from "./dto/user.dto";
import { User, UserDocument } from "./user.schema";

@Injectable()
export class UserService {

  constructor(@InjectModel(User.name) private readonly userModel: Model<UserDocument>) {}

  async createUser(createUserDTO: CreateUserDTO): Promise<User> {
    console.log(createUserDTO);
    
    const createdUser = new this.userModel(createUserDTO)
    return createdUser.save()
  }

  async findOneByEmail(email: string): Promise<UserDocument | null> {
    return await this.userModel.findOne({ email }).exec()
  }

  async findAll(): Promise<User[]> {
    return await this.userModel.find().exec()
  }

  async exists(email: string): Promise<boolean> {
    return await this.userModel.exists({ email })
  }
}