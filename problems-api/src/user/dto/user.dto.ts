export class CreateUserDTO {
  email: string
  firstName: string
  lastName: string
  grade: string
}

export class UserDTO {
  email: string
  firstName: string
  lastName: string
  grade: string
}