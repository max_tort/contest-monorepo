import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { LevelController } from "./level.controller";
import { Level, LevelSchema } from "./level.schema";
import { LevelService } from "./level.service";

@Module({
  imports: [MongooseModule.forFeature([{ name: Level.name, schema: LevelSchema }], 'native')],
  providers: [LevelService],
  controllers: [LevelController],
  exports: [LevelService]
})

export class LevelModule {}