import { Body, Controller, Get, Param, Post } from "@nestjs/common";
import { UserService } from "src/user/user.service";
import { Level, LevelDTO } from "./level.schema";
import { LevelService } from "./level.service";

@Controller('level')
export class LevelController {
  constructor(private levelService: LevelService) {}

  @Get('/findOne/:_id')
  async findOneByID(@Param('_id') _id: string): Promise<Level | null> {
    return await this.levelService.findOneByID(_id)
  }

  @Get('/findByContestID/:contestID')
  async findByContestID(@Param('contestID') contestID: string): Promise<Level[]> {
    return await this.levelService.findByContestID(contestID)
  }

  @Post('/create')
  async createLevel(@Body() levelDTO: LevelDTO): Promise<Level> {
    return await this.levelService.createLevel(levelDTO)
  }
}