import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Level, LevelDocument, LevelDTO } from "./level.schema";
import { Model } from "mongoose";

@Injectable()
export class LevelService {
  constructor(@InjectModel(Level.name) private levelModel: Model<LevelDocument>) {}

  async createLevel(levelDTO: LevelDTO): Promise<Level> {
    const newLevel = new this.levelModel(levelDTO)
    return await newLevel.save()
  }

  async findOneByID(_id: string): Promise<LevelDocument | null> {
    return this.levelModel.findById(_id).exec()
  }

  async findByContestID(contestID: string): Promise<Level[]> {
    return this.levelModel.find({contestID}).select('_id name').lean().exec()
  }

}