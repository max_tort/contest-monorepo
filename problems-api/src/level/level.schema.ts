import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export class Block {
  position: number[]
  color: string
}

export class Star {
  position: number[]
  isEaten?: boolean
}

export class Hero {
  position: number[]
  direction: number
}

export type Command = 'step' | 'left' | 'right' | 'paint' | 'call function' | 'recursion' | 'if'



export class LevelRestrinctions {
  allowedCommands: Command[]
  numberOfFunctions: number
  numberOfCommands: number
  numberOfCommandsInFunction?: number[]
}

export type LevelDocument = Level & Document

export class LevelDTO {
  name: string
  contestID: string
  blocks: Block[]
  start: Star[]
  hero: Hero
  restrictions: LevelRestrinctions
  goal: string
  weight: number
}

@Schema()
export class Level {

  @Prop({required: true})
  name: string
  
  @Prop({required: true})
  contestID: string

  @Prop([Block])
  blocks: Block[]

  @Prop([Star])
  stars: Star[]

  @Prop(Hero)
  hero: Hero

  @Prop(LevelRestrinctions)
  restrictions: LevelRestrinctions

  @Prop({default: "collectStars"})
  goal: string

  @Prop({required: true})
  weight: number

}

export const LevelSchema = SchemaFactory.createForClass(Level)

