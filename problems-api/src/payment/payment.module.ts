import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AuthModule } from "src/auth/auth.module";
import { ContestRegistrationModule } from "src/contest-registration/contest-registration.module";
import { ContestModule } from "src/contest/contest.module";
import { UserModule } from "src/user/user.module";
import { PaymentController } from "./payment.controller";
import { Payment, PaymentSchema } from "./payment.schema";
import { PaymentService } from "./payment.service";

@Module({
  imports: [AuthModule, UserModule, ContestModule, ContestRegistrationModule, MongooseModule.forFeature([{name: Payment.name, schema: PaymentSchema}], 'native')],
  providers: [
    PaymentService,
    {
      provide: 'crypto',
      useFactory: () => require('crypto-js')
    },
    {
      provide: 'Parser',
      useFactory: async () => new (await require('xml2js')).Parser()
    },
    {
      provide: 'Builder',
      useFactory: async () => new (await require('xml2js')).Builder()
    },
  ],
  controllers: [PaymentController],
  exports: [PaymentService]
})
export class PaymentModule { }