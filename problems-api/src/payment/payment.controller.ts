import { Body, Controller, Post, Request, UseGuards } from "@nestjs/common";
import { JwtAuthGuard } from "src/auth/jwt-auth.guard";
import { Payment } from "./payment.schema";
import { PaymentService } from "./payment.service";



@Controller('payment')
export class PaymentController {
  constructor(private paymentService: PaymentService) { }

  @UseGuards(JwtAuthGuard)
  @Post('create')
  async createPayment(@Request() request, @Body() { contestID }: { contestID: string}): Promise<string> {
    const payment = await this.paymentService.createPayment(request.user.email, contestID)
    return await this.paymentService.createPayboxPayment(payment, 'blah blah', 'sex sex')
  }
}