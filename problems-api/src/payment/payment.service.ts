import { Inject, Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { ContestRegistrationService } from "src/contest-registration/contest-registration.service";
import { Contest } from "src/contest/contest.schema";
import { ContestService } from "src/contest/contest.service";
import { User } from "src/user/user.schema";
import { UserService } from "src/user/user.service";
import { PayboxPaymentInfo, Payment, PaymentDocument } from "./payment.schema";
import { v4 as uuidv4 } from 'uuid'
import crypto from 'crypto-js'
import {
  Parser,
  Builder
} from 'xml2js'

@Injectable()
export class PaymentService {

  private secret: string
  private currency: string
  private merchantID: string
  private salt: string

  constructor(
    private configService: ConfigService,
    private userService: UserService,
    private contestService: ContestService,
    private contestRegistrationService: ContestRegistrationService,
    @InjectModel(Payment.name) private readonly paymentModel: Model<PaymentDocument>,
    @Inject('crypto') private readonly crypto: any,
    @Inject('Parser') private parser: Parser,
    @Inject('Builder') private builder: Builder
  ) {
    this.secret = this.configService.get('PG_SECRET')
    this.currency = this.configService.get('PG_CURRENCY') || 'USD'
    this.merchantID = this.configService.get('PG_MERCHANT_ID')
    this.salt = this.configService.get('PG_SALT') || 'none'
  }

  async createPayment(userEmail: string, contestID: string): Promise<PaymentDocument> {
    const user = await this.userService.findOneByEmail(userEmail)
    if (user) {
      const contest = await this.contestService.findByID(contestID)
      if (contest) {
        if (this.contestRegistrationService.isUserRegistered(user, contestID)) {
          const paymentDTO: Payment = {
            userID: user._id,
            contestID: contest._id,
            transactionID: uuidv4(),
            amount: contest.cost
          }
          return await new this.paymentModel(paymentDTO).save()
        } else throw new Error(`User ${userEmail} is not registered to the contest ${contestID}`)
      }
      else throw new Error(`Contest with ID: ${contestID} doesn't exist.`)
    }
    else throw new Error(`User with email: ${userEmail} doesn't exist.`)
  }

  async createPayboxPayment(payment: PaymentDocument, resultURL: string, successURL: string): Promise<string> {
    const paymentInfo: PayboxPaymentInfo = {
      pg_amount: payment.amount,
      pg_currency: this.currency,
      pg_description: 'Оплата за участие в Центрально-Азиатской Олимпиаде',
      pg_merchant_id: this.merchantID,
      pg_order_id: payment.transactionID,
      pg_result_url: resultURL,
      pg_salt: this.salt,
      pg_success_url: successURL
    }
    const conc: string = Object.keys(paymentInfo).sort().map(key => paymentInfo[key]).join(';')
    const requestXMLObject = {
      ...paymentInfo,
      pg_sig: this.crypto.MD5(conc).toString()
    }
    const myXML = this.builder.buildObject({request: requestXMLObject})
    console.log(myXML);
    return myXML
    
  }
}