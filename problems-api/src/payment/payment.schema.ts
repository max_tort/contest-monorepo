import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document, SchemaTypes, Types } from "mongoose";
import { Contest } from "src/contest/contest.schema";
import { User } from "src/user/user.schema";

export type PaymentDocument = Payment & Document
export type PaymentStatus = 'pending' | 'ok' | 'failed'

export class PayboxPaymentInfo {
  pg_amount: number
  pg_currency: string
  pg_description: string
  pg_merchant_id: string
  pg_order_id: string
  pg_result_url: string
  pg_salt: string
  pg_success_url: string
}

@Schema()
export class Payment {
  @Prop({ required: true, type: SchemaTypes.ObjectId, ref: User.name })
  userID: Types.ObjectId

  @Prop({ required: true, type: SchemaTypes.ObjectId, ref: Contest.name })
  contestID: Types.ObjectId

  @Prop({ required: true })
  transactionID: string

  @Prop({ required: true })
  amount: number

  @Prop({ default: 'pending' })
  status?: PaymentStatus
}

export const PaymentSchema = SchemaFactory.createForClass(Payment) 