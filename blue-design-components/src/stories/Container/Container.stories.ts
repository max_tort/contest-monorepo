import Container from './Container.svelte'
import ContainerView from './ContainerView.svelte'

export default {
  title: 'Example/Container',
  component: ContainerView,
  argTypes: {
    row: {
      defaultValue: true,
      control: {
        type: 'boolean',
      }
    }
  }
}

export const Basic = ({...args}) => ({
  props: args,
  Component: ContainerView
})