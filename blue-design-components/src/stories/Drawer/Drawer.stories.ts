import DrawerView from './DrawerView.svelte'

export default {
  component: DrawerView,
  title: 'Example/Drawer',
  argTypes: {
    padding: {
      defaultValue: 'medium',
      control: {
        type: 'radio',
        options: [
          'small',
          'medium',
          'large'
        ]
      },
    },
    // open: {
    //   defaultValue: true,
    //   control: {
    //     type: 'boolean'
    //   }
    // }
  }
}

export const Basic = ({...args}) => ({
  Component: DrawerView,
  props: args
})