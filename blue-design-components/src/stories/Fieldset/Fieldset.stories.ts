import FieldsetView from './FieldsetView.svelte'

export default {
  title: 'Example/Fieldset',
  component: FieldsetView,
  argTypes: {
    width: {
      defaultValue: 300,
      control: {
        type: 'range',
        min: 100,
        max: 1000,
        step: 10
      }
    },
    padding: {
      defaultValue: 'medium',
      control: {
        type: 'radio',
        options: [
          'small',
          'medium',
          'large'
        ]
      }
    }
  }
}

export const Basic = ({ ...args }) => ({
  Component: FieldsetView,
  props: args
})