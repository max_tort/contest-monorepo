import Fieldset from './Fieldset.svelte'
import Header from './Header.svelte'
import Content from './Content.svelte'
import Footer from './Footer.svelte'
import FooterStatus from './FooterStatus.svelte'
import FooterActions from './FooterActions.svelte'

export default {
  Fieldset,
  Header,
  Content,
  Footer,
  FooterStatus,
  FooterActions
}