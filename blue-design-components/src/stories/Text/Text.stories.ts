import TextView from './TextView.svelte'

export default {
  title: 'Example/Text',
  component: TextView,
  argTypes: {}
}

export const Basic = ({...args}) => ({
  Component: TextView,
  props: args
})