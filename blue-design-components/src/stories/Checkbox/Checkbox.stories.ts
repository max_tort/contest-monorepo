import Checkbox from './Checkbox.svelte'

export default {
  title: 'Example/Checkbox',

  component: Checkbox,
  argTypes: {
  }
}

export const Basic = ({ ...args }) => ({
  Component: Checkbox
})