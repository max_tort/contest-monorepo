import Button from './Button.svelte'
import ChevronLeft from '../Icons/ChevronLeft.svelte'

export default {
  title: "Blue Design Components/Button",
  component: Button,
  argTypes: {
    type: {
      defaultValue: 'primary',
      control: {
        type: "inline-radio",
        options: [
          'primary',
          'secondary',
          'success',
          'error',
          'alert',
          'warning',
          'violett',
        ],
      }
    },
    size: {
      defaultValue: 'medium',
      control: {
        type: "inline-radio",
        options: [
          'small',
          'medium',
          'large',
        ]
      },
      default: 'small'
    },
    disabled: {
      defaultValue: false,
      control: {
        type: "boolean"
      },
    },
    loading: {
      defaultValue: false,
      control: {
        type: 'boolean'
      },
    },
    width: {
      control: {
        type: 'range',
        min: 50,
        max: 600
      }
    }
  }
}

export const Primary = ({ ...args }) => ({
  Component: Button,
  props: args
})

export const WithIcon = ({ ...args }) => ({
  Component: Button,
  props: {
    ...args,
    prefix: ChevronLeft
  }
})