import sirv from 'sirv';
import polka from 'polka';
import compression from 'compression';
import * as sapper from '@sapper/server';
import cookie from 'cookie'
import dotenv from 'dotenv'
import { getHeaderData, redirectToMain } from './utility';
const cors = require('cors')({ origin:true });

// configuring .env file
dotenv.config()

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';

polka()
	.use(cors)
	.use('/getHeaderData/:key', getHeaderData)
	.use('/redirectToMain', redirectToMain)
	.use(
		compression({ threshold: 0 }),
		sirv('static', { dev }),
		sapper.middleware({
			session: (req) => {
				return {
					jwt: cookie.parse(req.headers.cookie)[process.env.COOKIE_KEY] || '',
					api: process.env.API_HOST || ''
				}
			}
		})
	).listen(PORT, err => {
		if (err) console.log('error', err);
	});
