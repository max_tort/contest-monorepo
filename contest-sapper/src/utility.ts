import type { ServerResponse } from "http"
import type { Request } from "polka"

export const getHeaderData = (req, res: ServerResponse, next) => {
  const authorization: string | undefined = req.params['key']
  if (authorization) {
    
    res.setHeader('Set-Cookie', [`${process.env.COOKIE_KEY}=${authorization}; Path=/`])
    res.setHeader(
      'Location', '/redirectToMain'
    )
    res.writeHead(302)
    res.end()
  } else {
    res.writeHead(400).end()
  }
}

export const redirectToMain = (req, res, next) => {
  
  res.writeHead(301, {
    'Location': '/',
  })
  res.end()
}
