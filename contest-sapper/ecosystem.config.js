module.exports = {
  apps : [{
	  name: 'contest-front-end',
    script: './__sapper__/build',
  },],

  deploy : {
    production : {
      user : 'root',
      host : '165.227.148.85',
      key: '/root/.ssh/id_rsa.pub',
      
      ref  : 'origin/master',
      repo : 'GIT_REPOSITORY',
      path : '/home/root/contest-monorepo',
      'pre-deploy-local': '',
      'post-deploy' : 'cd ./contest-sapper; npm install && npm run build && pm2 reload ecosystem.config.js',
      'pre-setup': ''
    }
  }
};
